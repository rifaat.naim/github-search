# Github Search

Github search is an application that let's user search for repositories in GitHub by topic.

## Installation

Make sure composer is installed in machine. 

Cloning repository

```bash
git clone https://gitlab.com/rifaat.naim/github-search.git
```

Navigate to project directory via terminal and install dependencies.
```
cd github-search
composer install
```

Copy and rename .env.example as .env

Insert at the bottom of .env following credentials:
```
GITHUB_USERNAME="INSERT_YOUR_GITHUB_USERNAME"
GITHUB_TOKEN="INSERT_YOUR_ACCESS_TOKEN"
```
Running below command in project base directory
```
php artisan key:generate
```

## Usage

```
php artisan serve
```

You may access the web at http://127.0.0.1:8000 (default). Or may access via different endpoint by specifying --host and --port flags. 
