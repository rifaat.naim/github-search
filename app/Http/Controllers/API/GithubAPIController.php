<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Elasticsearch\ClientBuilder;
use Carbon\Carbon;
use App\Search;

class GithubAPIController extends Controller
{
    protected $client   = null;
    protected $endpoint = 'https://api.github.com';
    protected $accessToken;
    protected $userName;

    public function __construct()
    {
        $this->accessToken  = config('services.github.token');
        $this->userName = env('GITHUB_USERNAME');
        $this->hosts = [
            '54.151.228.110:9200',         
            '54.151.228.110',              
            'https://localhost',        
            'https://54.151.228.110:9200'  
        ];
        $this->indexName = 'search3query';
    }

    public function searchTopics(Request $request)
    {
        $query = $request->input('searchQuery');
        $page = $request->input('page');
        $perPage = '30';
        $date = Carbon::now();
        $header = [
            'Accept: application/vnd.github.mercy-preview+json'
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_USERAGENT,'githubSearch');
        curl_setopt($curl, CURLOPT_URL, $this->endpoint . '/search/topics?q='.$query.'&page='.$page.'&per_page='.$perPage);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERPWD, $this->userName.':'.$this->accessToken);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $content = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($content);
        $client = ClientBuilder::create()->setHosts($this->hosts)->build();              
        $params = ['index' => $this->indexName];
        $bool = $client->indices()->exists($params);
        if($bool == 1)
        {
            $params = [
                'index' => $this->indexName,
                'body'  => [ 
                    'searchQuery' => $query,
                    'page' => $page,
                    'date' => $date
                    ]
            ];

            $response = $client->index($params);
        }
        else
        {
            $params = [
                'index' => $this->indexName,
                'body' => [
                    'settings' => [
                        'number_of_shards' => 3,
                        'number_of_replicas' => 2
                    ],
                    'mappings' => [
                        '_source' => [
                            'enabled' => true
                        ],
                        'properties' => [
                            'searchQuery' => [
                                'type' => 'keyword'
                            ],
                            'page' => [
                                'type' => 'keyword'
                            ],
                            'date' => [
                                'type' => 'keyword'
                            ]
                        ]
                    ]
                ]
            ];
            $response = $client->indices()->create($params);
        }


        return response()->json($res, 200);
    }

    public function getAllQueries()
    {
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "http://54.151.228.110:9200/".$this->indexName."/_search",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS =>"{\r\n  \"aggs\": {\r\n    \"genres\": {\r\n      \"terms\": { \"field\": \"searchQuery\" } \r\n    }\r\n  }\r\n}",
          CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
          ),
        ));
        
        $response = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($response);

        return response()->json($res, 200);
    }

    public function populateResult($searchQuery, $page)
    {
        $perPage = '30';
        $date = Carbon::now();
        $header = [
            'Accept: application/vnd.github.mercy-preview+json'
        ];
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_USERAGENT,'githubSearch');
        curl_setopt($curl, CURLOPT_URL, $this->endpoint . '/search/topics?q='.$searchQuery.'&page='.$page.'&per_page='.$perPage);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_USERPWD, $this->userName.':'.$this->accessToken);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        $content = curl_exec($curl);
        curl_close($curl);
        $res = json_decode($content);
        if(!empty($res))
        {
            $resp =  $res->items;
            $totalCount = $res->total_count;
            if(count($res->items) == 30)
            {
                $maximumPage = intval(ceil($res->total_count/count($res->items)));
                if($maximumPage >= 34)
                {
                    $maximumPage = 34; //Github allows 1000 search API only
                }
            }
            else
            {
               $maximumPage = $page; 
            }
        }
        else
        {
            $resp = null;
            $totalCount = null;
            $maximumPage = null;
        }
        return view('admin-result', compact('resp', 'maximumPage', 'totalCount'));
    }

}
