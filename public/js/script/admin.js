$(document).ready(function(){
    var host = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
    document.querySelector('#loading').classList.add('loading');
    $.ajax({
    type: 'GET',
    crossDomain: true,
    url: host+'/api/getAllQueries',
    })
    .done(function(data){ 
        document.querySelector('#loading').classList.remove('loading');
        $.each(data.aggregations.genres.buckets, function(i, object){
        const parent = document.createElement('div');
        const cardBody = document.createElement('div');
        const cardTitle = document.createElement('h5');
        const cardText = document.createElement('p');
        const button = document.createElement('a');
        var searchQuery = "Search Query: " + object.key;
        var searchHits = "Number of hits: " + object.doc_count;
        var buttonText = "Populate result ";
        parent.setAttribute("class","card");
        cardBody.setAttribute("class", "card-body");
        cardTitle.setAttribute("class", "card-title");
        cardText.setAttribute("class", "card-text");
        button.setAttribute("class", "btn btn-primary");
        button.setAttribute("href", "/admin/result/" +object.key +"/1")
        parent.prepend(cardBody);
        cardTitle.prepend(searchQuery);
        cardText.prepend(searchHits);
        button.prepend(buttonText);
        cardBody.prepend(button);
        cardBody.prepend(cardText);
        cardBody.prepend(cardTitle);
        $('#apiResult').append(parent.outerHTML);
        })
    })
    .fail(function() { 

    });
    return false;
});