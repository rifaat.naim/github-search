var timer = null;
$("#searchQuery").keydown(function(){
clearTimeout(timer);
timer = setTimeout(doStuff, 1000)
});
function doStuff() {
var host = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
document.getElementById("page").value = 1;
var searchVal = $( "input" ).serializeArray();
document.getElementById("searchQuery").value = searchVal[0].value;
document.querySelector('#loading').classList.add('loading');
$.ajax({
type: 'POST',
crossDomain: true,
url: host+'/api/search',
data: searchVal
})
.done(function(data){
    document.querySelector('#loading').classList.remove('loading');
    $('#resultCard').empty();
    $('#searchViewCount').css("display", "block");
    if (data.total_count !== undefined)
    {
        $('#searchViewCount').css("display", "block");
        $('#responseCount').html(data.total_count+" number of records");
    }
    else
    {
        $('#searchViewCount').css("display", "none");
        $('#loadButton').css("display", "none");
    }
    $.each(data.items, function(i, object){
        const parent = document.createElement('div');
        const child1 = document.createElement('ul');
        const nameList = document.createElement('li');
        const descriptionList = document.createElement('li');
        var name = "Name: " + object.name;
        var description = "Description: " + object.description;
        parent.setAttribute("class","card");
        child1.setAttribute("class","list-group list-flush");
        nameList.setAttribute("class","list-group-item");
        descriptionList.setAttribute("class","list-group-item");
        parent.prepend(child1);
        nameList.prepend(name);
        descriptionList.prepend(description);
        child1.prepend(descriptionList);
        child1.prepend(nameList);
        $('#resultCard').append(parent.outerHTML);
        document.getElementById('loadButton').style.display = "block";
    })
})
.fail(function() { 
    $('searchView').css("display", "block");
    $('$response').html("<b>Error encountered!</b>");

});
}

function loadMore()
{
$('#resultCard').empty();
var currentPage = document.getElementById("page").value;
var pageNumber = document.getElementById("page").value = parseInt(currentPage, 10) + 1;
var searchQuery = document.getElementById("searchQuery").value;
var host = location.protocol+'//'+location.hostname+(location.port ? ':'+location.port: '');
var searchData = [{
"name" : "searchQuery",
"value" : searchQuery
},
{
"name" : "page",
"value" : pageNumber
}
];
document.querySelector('#loading').classList.add('loading');
$.ajax({
type: 'POST',
crossDomain: true,
url: host+'/api/search',
data: searchData 
})
.done(function(data){ 
$.each(data.items, function(i, object){
    document.querySelector('#loading').classList.remove('loading');
    const parent = document.createElement('div');
    const child1 = document.createElement('ul');
    const nameList = document.createElement('li');
    const descriptionList = document.createElement('li');
    var name = "Name: " + object.name;
    var description = "Description: " + object.description;
    parent.setAttribute("class","card");
    child1.setAttribute("class","list-group list-flush");
    nameList.setAttribute("class","list-group-item");
    descriptionList.setAttribute("class","list-group-item");
    parent.prepend(child1);
    nameList.prepend(name);
    descriptionList.prepend(description);
    child1.prepend(descriptionList);
    child1.prepend(nameList);
    $('#resultCard').append(parent.outerHTML);
    document.getElementById('loadButton').style.display = "block";
})
})
.fail(function() { 
$('searchView').css("display", "block");
$('$response').html("<b>Error encountered!</b>");

});
return false;
}