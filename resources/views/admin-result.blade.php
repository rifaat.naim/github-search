<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin View</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
</head>
<style>
.bg-white{
    background-color: #fff;
}
.searchWidth{
    width: 600px;
    height: 50px;
}
</style>
<body class="bg-white">
<nav class="navbar navbar-light bg-light">
  <span class="navbar-brand mb-0 h1 my-auto"><a href="/admin"><i class="fa fa-chevron-left text-black mr-2"></i>Back</a></span>
</nav>
    <div class="container">
        <div class="row justify-content-center mb-4 mt-4">
            <h1>Admin Report | Populate Result</h1>
        </div>
        <div class="row justify-content-center mb-2">
            <div id="responseCount">
                {{ $totalCount }} number of records
            </div>
        </div>
        <div class="card-columns">
            @if(!empty($resp))
                @foreach($resp as $res)
                <div class="card">
                        <ul class="list-group list-flush">
                            <li class="list-group-item">
                                    Name: {{ $res->name }}
                            </li>
                            <li class="list-group-item">
                                    Description: {{ $res->description }}
                            </li>
                        </ul>
                </div>
                @endforeach
            @endif
        </div>
        <div class="row justify-content-center">

            <nav aria-label="Page navigation example text-center pb-4">
            <ul class="pagination">
                @if(Route::getCurrentRoute()->parameters['page'] != 1)
                    <li class="page-prev"><a class="page-link" href="/admin/result/{{Route::getCurrentRoute()->parameters['searchQuery']}}/{{Route::getCurrentRoute()->parameters['page'] - 1}}">Prev</a></li>
                @endif
                @if(Route::getCurrentRoute()->parameters['page'] != $maximumPage)
                    <li class="page-item"><a class="page-link" href="/admin/result/{{Route::getCurrentRoute()->parameters['searchQuery']}}/{{Route::getCurrentRoute()->parameters['page'] + 1}}">Next</a></li>
                @endif
                </ul>
            </nav>
        </div>
    </div>
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
</html>