<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Admin View</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
</head>
<style>
.bg-white{
    background-color: #fff;
}
.searchWidth{
    width: 600px;
    height: 50px;
}
.loading {
	z-index: 20;
	position: absolute;
	top: 0;
	left:-5px;
	width: 100%;
	height: 100%;
    background-color: rgba(0,0,0,0.4);
}

</style>
<body class="bg-white">
<section id="loading">
</section>
<nav class="navbar navbar-light bg-light">
  <span class="navbar-brand mb-0 h1 my-auto"><a href="/"><i class="fa fa-chevron-left text-black mr-2"></i>Search</a></span>
</nav>
    <div class="container">
        <div class="row justify-content-center mb-4 mt-4">
            <h1>Admin Report</h1>
        </div>
        <div class="card-columns" id="apiResult">
           
        </div>
        
    </div>
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
<script type="text/javascript" src="{{ asset('js/script/admin.js') }}"></script>
</html>