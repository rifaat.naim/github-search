<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Search Topics</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/fontawesome.min.css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-2/css/all.min.css" />
</head>
<style>
.bg-white{
    background-color: #fff;
}
.searchWidth{
    width: 600px;
    height: 50px;
}
.loading {
	z-index: 20;
	position: absolute;
	top: 0;
	left:-5px;
	width: 100%;
	height: 100%;
    background-color: rgba(0,0,0,0.4);
}

</style>
<body class="bg-white">
<section id="loading">
  </section>
<nav class="navbar navbar-light bg-light">
  <a class="navbar-brand h4">Github Topics</a>
  <form class="form-inline">
    <span class="my-2 my-sm-0 h4"><a href="/admin">Report<i class="fa fa-chevron-right text-black mr-2 ml-2"></i></a></span>
  </form>
</nav>
<div class="container">
    <div class="row mt-4">
        <h1 class="mx-auto">Search Topics on Github</h1>
    </div>
    <div class="row justify-content-center">
            <div class="form-row">
                <div class="col-md-12">
                    <div class="form-group">
                        <input type="text" name="searchQuery" class="form-control searchWidth" id="searchQuery" placeholder="Search a Topic" required>
                        <input type="text" value="1" name="page" id="page" hidden>
                    </div>
                </div>
            </div>
    </div>
    <div class="row justify-content-center">
        <div id="searchViewCount">
            <div id="responseCount">
            </div>
        </div>
    </div>

    <div class="row justify-content-start">
        <div class="card-columns" id="resultCard">

        </div>
    </div>
    
    <div class="row justify-content-center pb-4" id="loadPlace">
        <button class="btn btn-primary" style="display: none;" id="loadButton" onclick="loadMore()">Next Page</button>
    </div>
</div>


<script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/app.js') }}"></script>
</body>
<script type="text/javascript" src="{{ asset('js/script/search.js') }}"></script>
</html>